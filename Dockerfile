# sestaveni libmodsecurity a dynamickeho modulu ngx_http_modsecurity_module.so
ARG NGINX_VERSION="1.19.8"
ARG CRS_VERSION="3.3.0"

FROM nginx:${NGINX_VERSION}-alpine as build

# instalace zavislosti pro sestaveni libmodsecurity
RUN build_pkgs="alpine-sdk apr-dev apr-util-dev autoconf automake binutils-gold curl curl-dev g++ gcc geoip-dev git gnupg icu-dev libcurl libffi-dev libjpeg-turbo-dev libstdc++ libtool libxml2-dev linux-headers lmdb-dev m4 make openssh-client pcre-dev pcre2-dev perl pkgconf wget yajl-dev zlib-dev" && \
    apk add --update --no-cache ${build_pkgs}

ARG NGINX_VERSION
ARG CRS_VERSION

# stazeni zdrojovych kodu a core rule setu
RUN mkdir -p /src && \
    git clone --depth 1 -b v3/master --single-branch https://github.com/SpiderLabs/ModSecurity /src/ModSecurity && \
    git clone --depth 1 https://github.com/SpiderLabs/ModSecurity-nginx.git /src/ModSecurity-nginx && \
    echo "NGINX_VERSION: ${NGINX_VERSION}" && \
    echo "CRS_VERSION: ${CRS_VERSION}" && \
    wget -qO - https://nginx.org/download/nginx-${NGINX_VERSION}.tar.gz | tar xzf - -C /src && \
    wget -qO - https://github.com/coreruleset/coreruleset/archive/v${CRS_VERSION}.tar.gz | tar xzf - -C /src

# sestaveni libmodsecurity
RUN cd /src/ModSecurity && \
    git submodule init && \
    git submodule update && \
    ./build.sh && \
    ./configure && \
    make && \
    make install

# sestaveni dynamickeho modulu
RUN cd /src/nginx-${NGINX_VERSION} && \
    ./configure --with-compat --add-dynamic-module=../ModSecurity-nginx && \
    make modules

# pro samotne nasazeni jiz nepotrebujeme instalovane zavislosti pro kompilaci a zdrojove kody
FROM nginx:${NGINX_VERSION}-alpine

# instalace runtime zavislosti
RUN apk add --update --no-cache libcurl yajl libstdc++ libc6-compat curl jq supervisor && \
    mkdir /var/log/modsecurity /var/log/filebeat && \
    chown root:nginx /var/log/modsecurity /var/log/filebeat

# stazeni a instalace filebeatu
RUN cd /tmp && \
    wget https://artifacts.elastic.co/downloads/beats/filebeat/filebeat-7.10.2-linux-x86_64.tar.gz && \
    tar xzvf filebeat-7.10.2-linux-x86_64.tar.gz && \
    mv filebeat-7.10.2-linux-x86_64 /usr/share/filebeat && \
    mkdir /usr/share/filebeat/logs /usr/share/filebeat/data && \
    rm /tmp/*

# pridani filebeatu do PATH
ENV PATH $PATH:/usr/share/filebeat

ARG NGINX_VERSION
ARG CRS_VERSION

# kopirovani sestavenych souboru a core rule setu z build prostredi
COPY --from=build /usr/local/modsecurity /usr/local/modsecurity
COPY --from=build /src/nginx-${NGINX_VERSION}/objs/ngx_http_modsecurity_module.so /etc/nginx/modules
COPY --from=build /src/ModSecurity/unicode.mapping /etc/nginx/modsecurity/unicode.mapping
COPY --from=build /src/coreruleset-${CRS_VERSION}/crs-setup.conf.example /etc/nginx/coreruleset/coreruleset.conf
COPY --from=build /src/coreruleset-${CRS_VERSION}/rules /etc/nginx/coreruleset/rules

# kopirovani zakladni konfigurace
COPY container-files/modsecurity.conf /etc/nginx/modsecurity/modsecurity.conf
COPY container-files/filebeat.yml /usr/share/filebeat/filebeat.yml
COPY container-files/supervisord.conf /etc/supervisor/conf.d/supervisord.conf
COPY container-files/nginx.conf /etc/nginx/nginx.conf

RUN rm -rf /var/log/nginx/error.log /var/log/nginx/access.log

CMD ["/usr/bin/supervisord", "-c", "/etc/supervisor/conf.d/supervisord.conf"]
